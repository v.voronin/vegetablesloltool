using System.Linq;
using Etirps.RiZhi;
using Rofl.Reader;
using SFB;
using UnityEngine;

public class ReplaySelector : MonoBehaviour
{
    public void SelectReplay()
    {
        var files = StandaloneFileBrowser.OpenFilePanel("Open File", "", "rofl", false);
        if (files.Length == 0)
            return;

        var path = files.First();
        ReadReplay(path);
    }

    private static void ReadReplay(string path)
    {
        var log = new RiZhi();
        var reader = new ReplayReader(log);
        var info = reader.ReadFile(path).Result;
    }
}