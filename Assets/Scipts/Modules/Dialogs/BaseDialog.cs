using UnityEngine;

namespace TeamTracker.UI
{
    public class BaseDialog : MonoBehaviour
    {
        private void OnEnable() => Init();

        protected virtual void Init()
        {
        }
    }
}