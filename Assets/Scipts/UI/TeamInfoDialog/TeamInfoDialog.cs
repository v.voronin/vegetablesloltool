using System.Collections.Generic;
using TeamTracker.Data;
using UnityEngine;

namespace TeamTracker.UI
{
    public class TeamInfoDialog : BaseDialog
    {
        [SerializeField] private TextAsset _teamsTable;
        [SerializeField] private Transform _teamParent;
        [SerializeField] private TeamInfo _teamPrefab;

        protected override void Init() => ParseTable();

        private void ParseTable()
        {
            var teamsInfo = _teamsTable.text.Split('\n');
            var teamsData = new List<TeamData>();

            for (var index = 1; index < teamsInfo.Length; index++)
            {
                var teamInfo = teamsInfo[index];
                var info = teamInfo.Split(',');
                var teamName = info[0];
                var teamDivision = GetDivisionByString(info[1]);
                var teamRating = int.Parse(info[2]);
                var playerDatas = new List<PlayerData>();

                for (var i = 3; i < info.Length; i += 5)
                {
                    if (string.IsNullOrEmpty(info[i]))
                        break;

                    var playerData = new PlayerData(info[i],
                        info[i + 1],
                        info[i + 2],
                        info[i + 3],
                        int.Parse(info[i + 4])
                    );

                    playerDatas.Add(playerData);
                }

                var teamData = new TeamData(teamName, teamDivision, teamRating, playerDatas.ToArray());
                teamsData.Add(teamData);
            }

            Fill(teamsData);
        }

        private static Division GetDivisionByString(string division) =>
            division switch
            {
                "Младший" => Division.Junior,
                "Средний" => Division.Middle,
                "Старший" => Division.Senior,
                _ => Division.Senior
            };

        private void Fill(IEnumerable<TeamData> teamInfos)
        {
            foreach (var info in teamInfos)
            {
                var team = Instantiate(_teamPrefab, _teamParent);
                team.Fill(info);
            }
        }
    }
}