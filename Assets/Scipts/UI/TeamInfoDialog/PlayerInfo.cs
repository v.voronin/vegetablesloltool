using TeamTracker.Data;
using TMPro;
using UnityEngine;

namespace TeamTracker.UI
{
    public class PlayerInfo : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _name;
        [SerializeField] private TextMeshProUGUI _rating;

        public void Fill(PlayerData data)
        {
            _name.text = data.Name;
            _rating.text = data.Rating.ToString();
        }
    }
}