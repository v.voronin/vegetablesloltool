using TeamTracker.Data;
using TMPro;
using UnityEngine;

namespace TeamTracker.UI
{
    public class TeamInfo : MonoBehaviour
    {
        [SerializeField] private TeamPlayerInfo _playerPrefab;
        [SerializeField] private TextMeshProUGUI _name;
        [SerializeField] private TextMeshProUGUI _division;

        public void Fill(TeamData data)
        {
            _name.text = data.Name;
            _division.text = data.Division.ToString();

            foreach (var player in data.Players)
            {
                var playerInstance = Instantiate(_playerPrefab, transform);
                playerInstance.Fill(player);
            }
        }
    }
}