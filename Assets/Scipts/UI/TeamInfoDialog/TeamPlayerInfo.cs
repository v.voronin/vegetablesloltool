using TeamTracker.Data;
using TMPro;
using UnityEngine;

namespace TeamTracker.UI
{
    public class TeamPlayerInfo : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _name;

        private PlayerData _data;

        public void Fill(PlayerData data)
        {
            _name.text = data.Name;
            _data = data;
        }
    }
}