namespace TeamTracker.UI.GameInfoDialog
{
    internal class TeamData
    {
        public PlayerData[] PlayersData;

        public TeamData(PlayerData[] playersData) => PlayersData = playersData;
    }
}