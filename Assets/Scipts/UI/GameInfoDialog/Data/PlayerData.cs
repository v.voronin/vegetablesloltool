using UnityEngine;

namespace TeamTracker.UI.GameInfoDialog
{
    internal class PlayerData
    {
        public readonly Sprite Avatar;
        public readonly string PlayerName;
        public readonly string ChampionName;

        public readonly string Kda;

        public readonly Sprite[] Items;

        public readonly string Cs;
        public readonly string Gold;

        public PlayerData(Sprite avatar, string playerName, string championName, string kda, Sprite[] items, string cs, string gold)
        {
            Avatar = avatar;
            PlayerName = playerName;
            ChampionName = championName;
            Kda = kda;
            Items = items;
            Cs = cs;
            Gold = gold;
        }
    }
}