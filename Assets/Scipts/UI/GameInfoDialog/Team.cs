using UnityEngine;

namespace TeamTracker.UI.GameInfoDialog
{
    [AddComponentMenu("UI/GameInfo/GameInfoDialog_TeamData")]
    internal class Team : MonoBehaviour
    {
        [SerializeField] private PlayerScoreboard _playerScoreboard;

        public void SetData(TeamData data)
        {
            foreach (var playerData in data.PlayersData)
            {
                var scoreboard = Instantiate(_playerScoreboard, transform);
                scoreboard.SetData(playerData);
            }
        }
    }
}