using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TeamTracker.UI.GameInfoDialog
{
    internal class PlayerScoreboard : MonoBehaviour
    {
        [SerializeField] private Image _avatar;
        [SerializeField] private TextMeshProUGUI _playerName;
        [SerializeField] private TextMeshProUGUI _championName;
        
        [SerializeField] private TextMeshProUGUI _kda;
        
        [SerializeField] private Image[] _items;
        
        [SerializeField] private TextMeshProUGUI _cs;
        [SerializeField] private TextMeshProUGUI _gold;

        public void SetData(PlayerData data)
        {
            _avatar.sprite = data.Avatar;
            _playerName.text = data.PlayerName;
            _championName.text = data.ChampionName;
            _kda.text = data.Kda;

            for (var i = 0; i < data.Items.Length; i++)
            {
                _items[i].sprite = data.Items[i];
            }
            
            _cs.text = data.Cs;
            _gold.text = data.Gold;
        }
    }
}
