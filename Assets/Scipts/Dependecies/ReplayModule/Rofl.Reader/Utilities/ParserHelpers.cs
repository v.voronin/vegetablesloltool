﻿using Rofl.Reader.Models;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Rofl.Reader.Utilities
{
    public static class ParserHelpers
    {
        private static readonly byte[] ROFLMagic = { 0x52, 0x49, 0x4F, 0x54 };
        private static readonly byte[] LPRMagic = { 0x04, 0x00, 0x00, 0x00 };
        private static readonly byte[] LRFMagic = { 0x00, 0x0B, 0x01, 0x00 };

        public static byte[] ReadBytes(FileStream fileStream, int count)
        {
            var buffer = new byte[count];

            fileStream.ReadAsync(buffer, 0, count);

            return buffer;
        }

        public static byte[] ReadBytes(FileStream fileStream, int count, int startOffset,
            SeekOrigin origin)
        {
            var buffer = new byte[count];

            fileStream.Seek(startOffset, origin);
            fileStream.ReadAsync(buffer, 0, count);

            return buffer;
        }
    }
}