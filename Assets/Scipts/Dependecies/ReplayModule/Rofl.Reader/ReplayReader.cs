﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Etirps.RiZhi;
using Rofl.Reader.Models;
using Rofl.Reader.Models.Internal.ROFL;
using Rofl.Reader.Parsers;
using Rofl.Reader.Utilities;

namespace Rofl.Reader
{
    [SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>")]
    public class ReplayReader
    {
        private readonly RiZhi _log;

        public ReplayReader(RiZhi log)
        {
            _log = log;
        }

        public async Task<ReplayFile> ReadFile(string filePath)
        {
            // Make sure file exists
            if (string.IsNullOrEmpty(filePath))
            {
                _log.Error("File reference is null");
                throw new ArgumentNullException("File reference is null");
            }

            if (!File.Exists(filePath))
            {
                _log.Error("File path not found, does the file exist?");
                throw new FileNotFoundException("File path not found, does the file exist?");
            }

            // Match parsers to file types
            var result = ReadROFL(filePath);

            // Make some educated guesses
            var detailsInferrer = new GameDetailsInferrer();

            result.Players = result.BluePlayers.Union(result.RedPlayers).ToArray();

            try
            {
                result.MapId = detailsInferrer.InferMap(result.Players);
            }
            catch (ArgumentNullException ex)
            {
                _log.Warning("Could not infer map type\n" + ex);
                result.MapId = MapCode.Unknown;
            }
            
            result.MapName = detailsInferrer.GetMapName(result.MapId);
            result.IsBlueVictorious = detailsInferrer.InferBlueVictory(result.BluePlayers, result.RedPlayers);

            foreach (var player in result.Players)
            {
                player.Id = $"{result.MatchId}_{player.PlayerID}";
            }

            // Set the alternate name to the default
            result.AlternativeName = result.Name;

            return result;
        }

        private ReplayFile ReadROFL(string filePath)
        {
            // Create a new parser
            var roflParser = new ROFLParser();

            // Open file stream and parse
            var fileStream = new FileStream(filePath, FileMode.Open);
            var parseResult = (ROFLHeader) roflParser.ReadReplay(fileStream);

            // Create replay file based on header
            return new ReplayFile
            {
                Type = ReplayType.ROFL,
                Name = Path.GetFileNameWithoutExtension(filePath),
                Location = filePath,
                MatchId = parseResult.PayloadFields.MatchId.ToString(),
                GameDuration = TimeSpan.FromMilliseconds(parseResult.MatchMetadata.GameDuration),
                GameVersion = parseResult.MatchMetadata.GameVersion,
                BluePlayers = parseResult.MatchMetadata.BluePlayers ?? new Player[0],
                RedPlayers = parseResult.MatchMetadata.RedPlayers ?? new Player[0],
                RawJsonString = parseResult.RawJsonString
            };
        }
    }
}
