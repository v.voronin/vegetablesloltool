namespace TeamTracker.Data
{
    public enum Division
    {
        Junior = 0,
        Middle = 1,
        Senior = 2,
    }
}