namespace TeamTracker.Data
{
    public readonly struct TeamData
    {
        public readonly string Name;
        public readonly Division Division;
        public readonly int Rating;
        public readonly PlayerData[] Players;

        public TeamData(string name, Division division, int rating, PlayerData[] players)
        {
            Name = name;
            Division = division;
            Rating = rating;
            Players = players;
        }
    }
}