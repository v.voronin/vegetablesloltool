namespace TeamTracker.Data
{
    public readonly struct PlayerData
    {
        public readonly string Name;
        public readonly int Rating;
        public readonly string MainAccountLink;
        public readonly string SecondAccountLink;
        public readonly string VkLink;

        public PlayerData(string name, string mainAccountLink, string secondAccountLink, string vkLink, int rating)
        {
            Name = name;
            MainAccountLink = mainAccountLink;
            SecondAccountLink = secondAccountLink;
            VkLink = vkLink;
            Rating = rating;
        }
    }
}